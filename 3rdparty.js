/*
This source file contains all the 3rd party javascript 
snippets and libraries that helped bring this all together
*/

// Adapted from a JSFiddle comment attached to a StackOverflow answer
// Original SO Ans: http://stackoverflow.com/questions/5533297/color-coding-based-on-number
// JSFiddle: http://jsfiddle.net/amackay11/xtb51wed/883/
function RGBScale(val) {
	mid = 2.5;

	if (val < mid) {
		// red to yellow
		r = 255;
		g = Math.floor(255 - (255 * ((mid - val) / mid)));
	} else {
		// yellow to red
		g = 255;
		r = Math.floor(255 - (255 * ((val - mid) / mid)));
	}
	return "rgb(" + r + "," + g + ",0)";
}

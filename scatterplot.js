/*
Data format description
{
    "business_id":"encrypted business id",
    "name":"business name",
    "neighborhood":"hood name",
    "address":"full address",
    "city":"city",
    "state":"state -- if applicable --",
    "postal code":"postal code",
    "latitude":latitude,
    "longitude":longitude,
    "stars":star rating, rounded to half-stars,
    "review_count":number of reviews,
    "is_open":0/1 (closed/open),
    "attributes":["an array of strings: each array element is an attribute"],
    "categories":["an array of strings of business categories"],
    "hours":["an array of strings of business hours"],
    "type": "business"
}
*/

var view_data = new Array();
var map;
var num_points = 0;
var min_size = 2;
var max_size = 10;

// Adapted from an example project found here:
// https://bl.ocks.org/mbostock/899711
function _init() {
	// Create the Google Map element centered around Phoenix, AZ
	map = new google.maps.Map(d3.select("#map").node(), {
		zoom: 10,
		center: new google.maps.LatLng(33.4620087, -112.0783892),
		mapTypeId: google.maps.MapTypeId.TERRAIN
	});
}

const socket = new WebSocket('ws://techwiz.noip.me:9999');

function popMap() {
	var overlay = new google.maps.OverlayView();

	// Add the container when the overlay is added to the map.
	overlay.onAdd = function() {
		var layer = d3.select(this.getPanes().overlayLayer).append("div")
		.attr("class", "business");

		// Draw each marker as a separate SVG element.
		overlay.draw = function() {
			var projection = this.getProjection(),
			  padding = 20;

			var marker = layer.selectAll("svg")
			  .data(d3.entries(view_data))
			  .each(transform) // update existing markers
			.enter().append("svg")
			  .each(transform)
			  .attr("class", "marker");

			// Add a circle.
			marker.append("circle")
				// We make the circle radius based on how many reviews this business has gotten. (4-14px)
			  .attr("r", function(d) { return (10 * (d.value.review_count / max_size)) + 4 + "px"; })
			  .attr("cx", padding)
			  .attr("cy", padding)
			  .style("fill", function(d) {return RGBScale(d.value.stars)});

			function transform(d) {
				// Convert lat/long to (x,y) with respect to the map's DIV element
				var ll = new google.maps.LatLng(d.value.latitude, d.value.longitude);
				var p = projection.fromLatLngToDivPixel(ll);
				return d3.select(this)
					.style("left", (p.x - padding) + "px")
					.style("top", (p.y - padding) + "px");
			}
		}
	}
	
	// Bind our overlay to the map
	overlay.setMap(map);
}

socket.addEventListener('open', function (event) {
	// Hard coded request for data from Arizona (~40k points)
	// We could map the whole world, but these algos don't scale well
	socket.send("GETSTATEDATA:AZ");
});

socket.addEventListener('message', function (event) {
	if (event.data == 'done'){
		// Got all our data, time to populate the map with markers
		//console.log("finished getting data.")
		popMap();
		return;
	}
	var json = JSON.parse(event.data)
	if (json.cmin != undefined){
		num_points = json.count;
		min_size = json.cmin;
		max_size = json.cmax;
	} else {
		// Cut out any data with fewer than 200 reviews (performance)
		if (json.review_count > 200)
			view_data.push(json);
	}
});

// Add a DOM event listener to the Google Map element so we don't get a blank map
google.maps.event.addDomListener(window, "load", _init);
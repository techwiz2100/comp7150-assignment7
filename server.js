const WebSocket = require('ws');
const fs = require('fs');
const libs = require('./3rdparty');

/*
Data format description
{
    "business_id":"encrypted business id",
    "name":"business name",
    "neighborhood":"hood name",
    "address":"full address",
    "city":"city",
    "state":"state -- if applicable --",
    "postal code":"postal code",
    "latitude":latitude,
    "longitude":longitude,
    "stars":star rating, rounded to half-stars,
    "review_count":number of reviews,
    "is_open":0/1 (closed/open),
    "attributes":["an array of strings: each array element is an attribute"],
    "categories":["an array of strings of business categories"],
    "hours":["an array of strings of business hours"],
    "type": "business"
}
*/

// Raw data will be stored in this var
var yelp_data = new Array();
var data_MapByState = {
	"AZ": [],
	"BW": [],
	"EDH": [],
	"ELN": [],
	"ESX": [],
	"FAL": [],
	"FIF": [],
	"FLN": [],
	"HLD": [],
	"IL": [],
	"KHL": [],
	"MLN": [],
	"NC": [],
	"NI": [],
	"NLK": [],
	"NTH": [],
	"NV": [],
	"NY": [],
	"OH": [],
	"ON": [],
	"PA": [],
	"PKN": [],
	"QC": [],
	"SC": [],
	"SCB": [],
	"STG": [],
	"VT": [],
	"WI": [],
	"WLN": []
}
var data_categories = {};
// Global state of internal database
var database_ready = false;

// Asynchronously read in the file because the dataset is pretty large (~100MB)
fs.readFile('yelp_business.json', (err, data) => {
	if (err) throw err;
	
	// Once we've grabbed the file in memory,
	var lines = data.toString().split('\n');
	for (var idx in lines){
		var line = lines[idx];
		// Parse every line as an individual entity/object and 
		// push into the raw data buffer
		yelp_data.push(JSON.parse(line));
	}
	// Populate the maps for quick lookup and partitioning
	for (var idx in yelp_data) {
		var entry = yelp_data[idx];
		if (entry.state)
			// Bin all the entries from the same state
			data_MapByState[entry.state].push(entry);
		for (var idx2 in entry.categories) {
			// Binning for categorical data needed to produce bar chart
			var category = entry.categories[idx2];
			if (data_categories[category] == undefined) data_categories[category] = {name:category, count:0, sum_rating:0, average_rating:0};
			++data_categories[category].count;
			data_categories[category].sum_rating += entry.stars;
		}
	}
	for (var idx in data_categories) {
		// We can't transfer a func over WebSocket, so let's make the average_rating value flat.
		var category = data_categories[idx];
		category.average_rating = category.count ? category.sum_rating/category.count : 0;
	}
	database_ready = true;
});

function sendData (ws, state) {
	if (state == undefined) {
		// Raw data
		for (var idx in yelp_data) {
			var entry = yelp_data[idx];
			ws.send(JSON.stringify(entry));
		}
		ws.send("done");
	} else if (state == "BAR") {
		// Categorical data
		for (var idx in data_categories) {
			var category = data_categories[idx];
			ws.send(JSON.stringify(category));
		}
		ws.send("done");
	} else {
		var count_min = 999999, count_max = 0, count_sum = 0, count = 0;
		// Mapped data by state
		for (var idx in data_MapByState[state]) {
			var entry = data_MapByState[state][idx];
			ws.send(JSON.stringify(entry));
			if (entry.review_count < count_min)
				count_min = entry.review_count;
			if (entry.review_count > count_max)
				count_max = entry.review_count;
			count_sum += entry.review_count;
			++count;
		}
		// Send over some metrics to make scaling calculations easier
		ws.send(JSON.stringify({"count":count, "cmax":count_max, "cmin":count_min, "avg_c":count_sum/count}));
		ws.send("done");
	}
}

// In the meantime, we setup a WebSocket to then communicate with the browser.
const wss = new WebSocket.Server({
	perMessageDeflate: false,
	port: 9999
});

// Create event listerner for a connection on socket
wss.on('connection', function(ws) {
	// Event listener for when a browser writes something on the connected socket.
	ws.on('message', function incoming(message){
		var date = new Date();
		console.log('[%s]: received: %s', date.toLocaleString(), message);
		if (!database_ready) {
			ws.send("DATABASE_NOT_READY");
			return;
		}
		
		// This server accepts the following commands via WebSocket:
		//   GETBARDATA - Returns the pre-computed bar chart data
		//   GETRAW - Returns the untouched Yelp data
		//   GETSTATEDATA - Returns the data associated with the specified state
		//
		// Server otherwise returns INVALID_CMD
		var command = message.split(':');
		switch (command[0]) {
		case 'GETSTATEDATA':
			sendData(ws, command[1]);
			break;
		case 'GETBARDATA':
			sendData(ws, "BAR");
			break;
		case 'GETRAW':
			sendData(ws);
			break;
		default:
			ws.send("INVALID_CMD");
		}
	});
});